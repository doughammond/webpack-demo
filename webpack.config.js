module.exports = {
	entry: './src/app.js',
	output: {
		filename: './build/js/app.js'
	},

	module: {
		preLoaders: [
			{
				test: /\.js$/, // include .js files
				exclude: /node_modules/, // exclude any and all files in the node_modules folder
				loader: "jshint-loader"
			}
		],
		loaders: [
			{
				test: /\.scss$/,
				loaders: ["style", "css", "sass"]
			}
		]
	}
};