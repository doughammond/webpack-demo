define([
	'./utils',
	'./submodule/widget',
	'../assets/scss/main.scss'
],
function(utils, widget) {
	'use strict';

	console.log(utils.foo(1, 2));
	console.log(widget.bar(3, 9));
});
