define([], function() {
	var exports = {};

	exports.foo = function(a, b) {
		return 'foo: ' + a + ' : ' + b;
	};

	return exports;
});
