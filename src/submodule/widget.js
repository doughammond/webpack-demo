define([], function() {
	var exports = {};

	exports.bar = function(a, b) {
		return 'bar: ' + a + ' : ' + (a+b);
	};

	return exports;
});
